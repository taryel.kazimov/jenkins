sudo systemctl status firewalld
# domain.cnf FAYLINI YARADIRIQ VE DAXILINDE BIZE UYGUN DEYISIKLIKLER EDIRIK.
# ASAGIDAKI SETIRIN KOMEYI ILE SERTIFIKAT YARADIRIQ. SONDA domain.cnf FAYLINDAN MELUMATLARIN GOTURULMESI NEZERDE TUTULUR.

mkdir certs && cd certs

vim domain.cnf

openssl req -new -x509 -newkey rsa:2048 -sha256 -nodes -keyout jenkins.contoso.com.key -days 3650 -out jenkins.contoso.com.crt -config domain.cnf

sudo yum install epel-release
sudo yum install nginx

sudo mkdir -p /etc/nginx/.ssl

sudo cp ~/certs/jenkins.contoso.com.* /etc/nginx/.ssl/

#  SERTIFIKATLAR UCUN SIMLINK YARADIRIQ.

sudo ln -s jenkins.contoso.com.crt cert.crt
sudo ln -s jenkins.contoso.com.key cert.key

# NGINX JENKINS UCUN NGINX CONFIG FAYLINI YARADIRIQ.

sudo vim /etc/nginx/conf.d/jenkins.conf

sudo systemctl start nginx
sudo systemctl enable nginx
sudo systemctl status nginx

# DISABLE PORT 8080 WITH FIREWALL OR EDIT JENKINS config file

sudo vim /usr/lib/systemd/system/jenkins.service

Environment="JENKINS_LISTEN_ADDRESS=127.0.0.1"

sudo systemctl daemon-reload

sudo systemctl restart jenkins