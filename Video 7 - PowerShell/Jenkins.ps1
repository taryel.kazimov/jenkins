# Ensure the build fails if there is a problem.
# The build will fail if there are any errors on the remote machine too!
$ErrorActionPreference = 'Stop'

# Create a password variable using the stored password from the Jenkins Global Passwords
$SecurePassword = $env:password | ConvertTo-SecureString -AsPlainText -Force


# If remoting to a machine with domain credentials, use "DOMAIN\YourUserName"
# If remoting to a machine on a workgroup, use "\YourUserName"
$User = "CONTOSO\ulduz"

# Create a PSCredential Object using the a hardcorded username and and $SecurePassword variable

$cred = New-Object System.Management.Automation.PSCredential -ArgumentList $User, $SecurePassword

Invoke-Command -FilePath  "$ENV:WORKSPACE\CreateUser.ps1" -ArgumentList $env:GivenName, $env:Surname, $env:title, $env:NewAccountPassword -Credential $cred -ComputerName dc01.contoso.com