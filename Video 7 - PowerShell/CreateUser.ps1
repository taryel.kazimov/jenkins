﻿param(
    [string]$GivenName,
    [string]$Surname,
    [string]$title,
    [string]$NewAccountPassword,
    [string]$department,
    [string]$Office,
    [string]$EmployeeID
    )

$DC = (Get-ADDomain).PDCEmulator
#$GivenName = "GivenName"
#$Surname = "Surname"
#$Password="5ftjbXg9FaZh"
$SamAccountName=$GivenName+"."+$Surname
#$title = "System Engineer"
#$department = "IT Operations"
#$Office = "Malibu Point"
#$EmployeeID = "123456"
$departmentEnd = $department.Substring(0,[math]::Min($department.Length,64))
$UPN=$SamAccountName+"@"+(Get-ADDomain).forest
$Name=$GivenName+" "+$Surname
$DisplayName=$Name


$User = Get-ADUser -LDAPFilter "(sAMAccountName=$SamAccountName)"
If ($User -eq $Null) {

        New-ADUser -Name "$Name" `
        -Server $DC `
        -GivenName $GivenName `
        -Surname $Surname `
        -DisplayName "$DisplayName" `
        -SamAccountName $SamAccountName `
        -UserPrincipalName $UPN `
        -Enabled $true `
        -ChangePasswordAtLogon $true `
        -accountPassword (ConvertTo-SecureString -AsPlainText $NewAccountPassword -Force) `
        -Path "OU=USERS,OU=CONTOSO,DC=contoso,DC=com" `
        -Title $title `
        -Department "$departmentEnd" `
        -Office "$Office" `
        -EmployeeID "$EmployeeID" `

    }
    

Else {

        Send-MailMessage `
        -SmtpServer mail.contoso.com `
        -To 'clark.kent@contoso.com' `
        -From 'ActiveDirectory@contoso.com' `
        -Subject "$SamAccountName hesabı haqqında məlumat" `
        -Body "$SamAccountName istifadəçi hesabı artıq mövcuddur" `
        -Encoding 'UTF8'

}