# JENKINS CENTOS 7 UZERINE QURASDIRILMASI: https://www.jenkins.io/doc/book/installing/linux/#red-hat-centos

sudo wget -O /etc/yum.repos.d/jenkins.repo \
    https://pkg.jenkins.io/redhat-stable/jenkins.repo
    
sudo rpm --import https://pkg.jenkins.io/redhat-stable/jenkins.io.key


# JENKINS UCUN JAVA - NIN QURASDIRILMASI
sudo yum install java-11-openjdk

# SISTEMDE MOVCUD OLAN JAVANIN VERSIYASININ DEYISDIRILMESI
alternatives --config java

# JENKINSIN QURASDIRILMASI
sudo yum install jenkins
sudo systemctl daemon-reload

# jenkins servisini restart sonrai avtomatik calismasi ucun ayarlayiriq.
sudo systemctl enable jenkins
sudo systemctl start jenkins
sudo systemctl status jenkins


